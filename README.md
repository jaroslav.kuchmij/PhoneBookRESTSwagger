# Project "PhoneBook on gRPC + REST + Swagger"

## Данный репозиторий содержит самостойтельный проект на GO

### Условия проекта
1. Проект создать на Cobra
2. Использовать ProtoBuf для сириализации данных
3. Использовать BoltDB для хранения данных в базе данных
4. Использовать gRPC для создания клиент-серверного приложения
6. Использовать REST для работы через web
7. Использовать Swagger для UI REST и документации

### Инструкция по созданию проекта
1. Установка необходимых инструментов:
    - установка Cobra. В терминале
    <pre>
    go get github.com/spf13/cobra/cobra
    </pre>
    Подробо о том, как создать проект на Cobra можно ознакомится по [*ссылке*] (https://gitlab.com/jaroslav.kuchmij/phoneBookCobraSruct)
    - установка ProtoBuf. В терминале:
    <pre>
    go get github.com/golang/protobuf/protoc-gen-go
    </pre>
    Подробо о том, как создать проект c использованием ProtoBuf можно ознакомится по [*ссылке*] (https://gitlab.com/jaroslav.kuchmij/phoneBookProtoBuf)
    - установка BoltDB. В терминале
    <pre>
    go get github.com/boltdb/bolt/...
    </pre>
    - установк gRPC. В терминале
    <pre>
    go get google.golang.org/grpc
    </pre>
    - Импорт библиотеки для REST.
    <pre>
    go get github.com/gorilla/mux
    go get -u github.com/grpc-ecosystem/grpc-gateway/...
    </pre>
    - Импорт необходимых библиотек для Swagger
    <pre>
    go get github.com/jteeuwen/go-bindata
    go get github.com/elazarl/go-bindata-assetfs
    </pre>
    <pre>
    go install github.com/jteeuwen/go-bindata/go-bindata
    go install github.com/elazarl/go-bindata-assetfs/go-bindata-assetfs
    </pre>
2. Создать  проект COBRA - серверную  часть. Перейти в терминале %GOPATH%\bin и набрать команду:
    <pre>
    cobra init %GOPATH%/[Путь к проекту]/server 
    </pre>
3. в проекте создать proto файлы: структуру справочника и процедуры с логикой под телефонный справочник
    Пример структуры:
    <pre>
    syntax = "proto3";
    package models;
    message Contact {
    	string number = 1;
    	string name = 2;
        message Address {
            string street = 1;
            string numberHouse = 2;
            string numberFlat = 3;
        }
        Address address = 3;
    }
    message PhoneNumber {
        string number = 1;
    }
        </pre>
        
    Пример  прото файла с процедурами справочника:
    <pre>
    syntax = "proto3";
    package rrs;
    import "google/api/annotations.proto";// импорт функционала для реализации REST
    import "gitlab.com/jaroslav.kuchmij/PhoneBookRESTSwagger.git/models/phoneBookStruct.proto";// импорт структуры справочника
    import "github.com/golang/protobuf/ptypes/empty/empty.proto";// импорт пустой стрруктуры 
    import "github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger/options/annotations.proto"; // необходим для генерации документации для Swagger
    //Описание нашего справочника для Swagger
    option (grpc.gateway.protoc_gen_swagger.options.openapiv2_swagger) = {
        info {
            title: "PhoneBook Service"
            version: "1.0"
        }
        schemes: HTTP
        consumes: "application/json"
        produces: "application/json"
    };
    service PhoneBook {
        // Описание service для Swagger
    	option (grpc.gateway.protoc_gen_swagger.options.openapiv2_tag) = {
    		description: "PhoneBook to proto getAllPhonebook AddContact DeleteContact SearchContact UpdateContact"
    		external_docs {
    			url: "url"
                description: "Find out more about PhoneBookService"
            }
        };
        //процедура получения всего справочника
    	rpc GetAllPhonebook(google.protobuf.Empty) returns (stream ContactResponse) {
    		//Реализация логики Rest
    		option (google.api.http) = {
                get: "/phonebook/contacts"
            };
    		option (grpc.gateway.protoc_gen_swagger.options.openapiv2_operation) = {
    		  summary : "GetAllPhoneBook summary"
    		  description : "GetAllPhoneBook description"
    		  external_docs {
    		        url: "https://phonebook.pb.ua"
                    description: "Find out more about getAllContact"
                }
            };
    	}
    	//процедра добавление контакта
    	rpc AddContact (ContactRequest) returns (MessResponse) {
    		//Реализация логики Rest
    		option (google.api.http) = {
                post: "/phonebook/contacts"
                body: "*"
            };
    		option (grpc.gateway.protoc_gen_swagger.options.openapiv2_operation) = {
    		  summary : "AddContact summary"
    		  description : "AddContact description"
    		  external_docs {
    		        url: "https://phonebook.pb.ua"
                    description: "Find out more about CreateContact"
                }
            };
    	}
    }
    //структура ответа сервера
    message ContactResponse {
    	option (grpc.gateway.protoc_gen_swagger.options.openapiv2_schema) = {
            external_docs {
    			 url: "https://ContactResponse"
                description: "Find out more about ContactResponse"
            }
            json_schema {
                title : "ContactResponseSheme"
                description : "json_schema description"
                default : '{ message: "qwerty" }'
            }
        };
    	models.Contact contact = 1;
    }
    //структура ответа сервера
    message MessResponse {
    	option (grpc.gateway.protoc_gen_swagger.options.openapiv2_schema) = {
            external_docs {
    			 url: "https://MessResponse"
                description: "Find out more about ContactResponse"
            }
            json_schema {
                title : "MessResponseSheme"
                description : "json_schema description"
                default : '{ message: "qwerty" }'
            }
        };
    	string message = 2;
    }
    // Структура запроса
    message  ContactRequest{
    	models.Contact contact = 1;
    }
    </pre>

4. Генерация файлов из proto. Перейти в $GOPATH/bin/protoc-3.5.0-win32/bin и запустить команды генерациии. Ниже приведен пример моих команд (неоходимо изменить ваш локальный путь к файлам)
    <pre>
    protoc -I=E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookRESTSwagger.git -I=%GOPATH%\src\github.com\grpc-ecosystem\grpc-gateway -I=%GOPATH%\src\github.com\googleapis\googleapis -I=%GOPATH%\src\ --go_out=plugins=grpc:E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookRESTSwagger.git E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookRESTSwagger.git\models\phoneBookStruct.proto
    protoc -I=E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookRESTSwagger.git -I=%GOPATH%\src\github.com\grpc-ecosystem\grpc-gateway -I=%GOPATH%\src\github.com\googleapis\googleapis -I=%GOPATH%\src\ --go_out=plugins=grpc:E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookRESTSwagger.git E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookRESTSwagger.git\rrs\phoneBookRRS.proto
    protoc -I=E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookRESTSwagger.git -I=%GOPATH%\src\github.com\grpc-ecosystem\grpc-gateway -I=%GOPATH%\src\github.com\googleapis\googleapis -I=%GOPATH%\src\ --grpc-gateway_out=logtostderr=true:E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookRESTSwagger.git E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookRESTSwagger.git\rrs\phoneBookRRS.proto
    protoc -I=E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookRESTSwagger.git -I=%GOPATH%\src\github.com\grpc-ecosystem\grpc-gateway -I=%GOPATH%\src\github.com\googleapis\googleapis -I=%GOPATH%\src\ --swagger_out=logtostderr=true:E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookRESTSwagger.git E:\golang\project\src\gitlab.com\jaroslav.kuchmij\phoneBookRESTSwagger.git\rrs\phoneBookRRS.proto
    </pre>
5. Создание серверной части:
    - переходим в сгенерированный Cobra пакет
    - реализовываем функционал запуска серверов: GRSP и HTTP
    - имплиминтируем процедуры по созданию, удалению, изменению, чтению контактов из созданного нами прото файла
    - реализовываем логику хранения, записи, чтения, удаления из БД
    - создаем и выносим отдельно доп функционал (например валидация вх. параметров)
    - создаем cobra комманды запуска серверов
6. Создание Swagger
    - копируем в проект статическую часть swagger, которая реализовывает интерфейс Swagger
    - копируем файл json из скомпилированных файлов прото файла с процедурами.
    - в статическом файле index.html прописываем путь к json файлу
    - в корне пакета с ui из терминала запускаем команду:
    <pre>
    go-bindata-assetfs -pkg=swaggerui static/...
    </pre>

### Запуск проекта
1. Скачать проект - 
<pre>
go get gitlab.com/jaroslav.kuchmij/phoneBookRESTSwagger.git
</pre>
2. Перейти в пакет:  %GOPATH%/src/gitlab.com/jaroslav.kuchmij/phoneBookRESTSwagger.git/server и выполнить команду:
<pre>
go run main.go startGRPC
</pre>
После чего, необходимо ввести:
    - My.db - если запускаем боевую базу данных
    - Test.db - если запускаем тестовую БД
3. Открыть новое окно терминала, перейти в пакет:  %GOPATH%/src/gitlab.com/jaroslav.kuchmij/phoneBookRESTSwagger.git/server и выполнить команду:
<pre>
go run main.go startHTTP
</pre>
4. Открыть браузер. Ввести в адресное поле: http://localhost:8080/swaggerui/#/
    Результатом вашего запроса, должно быть отражение UI Swagger. Через него можно вызывать все REST команды приложеия

### Доп информация: 

    Перечень REST запросов:
    
####        - GET http://localhost:8080/phonebook/contacts - получение списка контактов
####        - POST http://localhost:8080/phonebook/contacts{body} - добаление нового контакта
####        - PUT http://localhost:8080/phonebook/contacts/{body} - изменение контакта (поиск по полю {"number")
####        - GET http://localhost:8080/phonebook/contacts/{namber} - поиск контакта по номеру телефона
####        - DELETE http://localhost:8080/phonebook/contacts/{namber} - удаление контакта по номеру телефона
        
    *Формат body: {"contact":{"number":"string","name":"string","address":{"City":"string","Street":"string","Building":"string","Apartment":"string"}}};
    
    **Формат поля "number" = '^[+][3][8][0][0-9\\s\\(\\)\\+]{9}$' (например: +380981111111)



### gRPC

[*Документация*](https://godoc.org/google.golang.org/grpc)

### RESTful API

[*Документация*](http://www.restapitutorial.ru/)

### Swagger UI

[*Сайт*](https://swagger.io/)

### Дополнительные материалы

http://mycodesmells.com/post/http-server-from-grpc-with-gateway

https://habrahabr.ru/post/337716/

https://grpc.io/blog/coreos

https://www.codementor.io/codehakase/building-a-restful-api-with-golang-a6yivzqdo

https://pdoviet.wordpress.com/2017/03/06/a-simple-api-using-protobuf-and-grpc/

https://cloud.google.com/endpoints/docs/grpc/transcoding



**~~Создатель кода~~** Главный бездельник - [*YaroslavKuchmiy*](https://www.facebook.com/profile.php?id=100006520172879)