package utils

import "regexp"

//Проверка входящего номера на корректность
func CheckValidNumber(incomingValue string) bool {
	var validID = regexp.MustCompile(`^[+][3][8][0][0-9\\s\\(\\)\\+]{9}$`) //формат номера
	res := validID.MatchString(incomingValue)
	return res
}
