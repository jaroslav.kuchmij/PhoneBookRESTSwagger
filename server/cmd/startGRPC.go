package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/PhoneBookRESTSwagger.git/server/serverAction"
	"fmt"
	"log"
	"gitlab.com/jaroslav.kuchmij/PhoneBookRESTSwagger.git/server/dao"
)

func init() {
	rootCmd.AddCommand(startGRSPCmd)
}

// Команда запуска сервера
var startGRSPCmd = &cobra.Command{
	Use:   "startGRPC",
	Short: "start grpc server",
	Long:  `This command starting grpc server.`,
	Run: func(cmd *cobra.Command, args []string) {
		//Выбор базы данных
		fmt.Println("Enter the name data base. Enter 'Test.db' if you run tests and 'My.db' if you run a combat DB")
		fmt.Scanln(&dao.NameDB)
		if dao.NameDB != "Test.db" && dao.NameDB != "My.db"  {
		log.Fatalf("Not a valid name data base")
		}

		serverAction.RunGRPC()
	},
}

