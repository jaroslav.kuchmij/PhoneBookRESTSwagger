package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/jaroslav.kuchmij/PhoneBookRESTSwagger.git/server/serverAction"
)

func init() {
	rootCmd.AddCommand(startHTTPCmd)
}

// Команда запуска сервера
var startHTTPCmd = &cobra.Command{
	Use:   "startHTTP",
	Short: "start HTTP server",
	Long:  `This command starting HTTP server.`,
	Run: func(cmd *cobra.Command, args []string) {
		serverAction.RunHTTP()
	},
}
