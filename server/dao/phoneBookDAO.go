package dao

import (
	"github.com/boltdb/bolt"
	"gitlab.com/jaroslav.kuchmij/PhoneBookRESTSwagger.git/models"
	"github.com/golang/protobuf/proto"
)

var (
	NameDB string
	DB *bolt.DB
)
const NameBucket = "PhoneBook"

//Открытие БД
func OpenDB() (error, *bolt.DB) {
	var err error
	DB, err = bolt.Open(NameDB, 0600, nil) //открытие БД
	if err != nil{
		return err, nil
	}
	return err, DB
}

//Создание корзины в БД
func CreateBucket() error {
	err := DB.Update(func(tx *bolt.Tx) error { //изменение БД
		_, err := tx.CreateBucketIfNotExists([]byte(NameBucket)) //создание Bucket
		if err != nil {
			return err
		}
		return err
	})
	if err != nil {
		return err
	}
	return err
}

//Добавление/изменение данных в корзине/корзину
func UpdateBucket(contact *models.Contact) error {
	mrshlOut, err := proto.Marshal(contact) //перевод Структуры в формат ProtoBuf (сирилизация)
	if err != nil{
		return err
	}
	err = DB.Update(func(tx *bolt.Tx) error { //Изменение БД
		err := tx.Bucket([]byte(NameBucket)).Put([]byte(contact.Number), mrshlOut) //Получение необходимой корзины и добавление новой записи
		if err != nil {
			return err
		}
		return err
	})
	if err != nil {
		return err
	}
	return err
}

//Проверка корзины на совпадение переданного значения
func ViewBucketForCoincidence(incomingValue string) (error, bool, *models.Contact)  {
	contact := models.Contact{}
	var boolSwitch bool = false
	err := DB.View(func(tx *bolt.Tx) error { //Открытие БД для чтения
		if val := tx.Bucket([]byte(NameBucket)).Get([]byte(incomingValue)); val != nil { //Получение значения по ключу
			err := proto.Unmarshal(val, &contact) //Анмаршал значения и запись в структуру данных
			if err != nil{
				panic(err)
			}
			boolSwitch = true // если есть совпадение указываем Истину
			return nil
		}
		return nil
	})
	if err != nil {
		return err, boolSwitch, &contact
	}
	return err, boolSwitch, &contact
}

//Удаление элемента из корзины по входящему ключу
func DeleteElementInBucket(number string) error {
	err := DB.Update(func(tx *bolt.Tx) error { //зменение корзины
		bckt := tx.Bucket([]byte(NameBucket)) //Получение корзины по наименованию
		err := bckt.Delete([]byte(number)) //Удаление записи в корзине по ключу
		return err
	})
	return err
}