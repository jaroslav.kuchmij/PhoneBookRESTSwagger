package serverAction

import (
	"log"
	"gitlab.com/jaroslav.kuchmij/PhoneBookRESTSwagger.git/server/dao"
	"gitlab.com/jaroslav.kuchmij/PhoneBookRESTSwagger.git/models"
	"gitlab.com/jaroslav.kuchmij/PhoneBookRESTSwagger.git/rrs"
	"context"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/golang/protobuf/proto"
	"errors"
	"gitlab.com/jaroslav.kuchmij/PhoneBookRESTSwagger.git/server/utils"
	"github.com/boltdb/bolt"
)

var (
	//положительные ответы сервера
	goodCreateResponse = &rrs.MessResponse{Message: "201 Created"}
	goodResponse = &rrs.MessResponse{Message: "200 Ok"}

	//отрицательные ответы сервера по вине клиента
 	badClientResponse = &rrs.MessResponse{Message: "400 Bad Request"}
	numberExistResponse = &rrs.MessResponse{Message: "404 This number exist"}
	numberNotFoundResponse = &rrs.MessResponse{Message: "404 Not Found"}

	//отрицательные ответы сервера по вине сервера
	badServerResponse = &rrs.MessResponse{Message: "500 Internal Server Error"}
)

//Добавление контакта
func (s server) AddContact(ctx context.Context, in *rrs.ContactRequest) (*rrs.MessResponse, error)  {
	log.Println("Add contact...")

	//Проверка валидности номера
	if !utils.CheckValidNumber(in.Contact.Number) {		//проверка на формат номера
		log.Println ("\t error in check validate number: ", errors.New("Invalid number"))
		return badClientResponse, nil
	}

	//Открытие БД
	err, db := dao.OpenDB()
	if err != nil {
		log.Println("\t error in Open DB: ", err)
		return badServerResponse, err
	}
	defer db.Close()//После выполнения команды - закрытие БД

	//Создание корзины, если ее нет
	err = dao.CreateBucket()
	if err != nil {
		log.Println ("\t error in Create Bucket in DB: ", err)
		return badServerResponse, err
	}

	//проверка на существование номера
	if err, bool, _ := dao.ViewBucketForCoincidence(in.Contact.Number); bool == true {
		if err != nil {
			log.Println("\t error in view DB: ", err)
			return badServerResponse, err
		}
		log.Println("\t error in view DB: ", errors.New("This number exist"))
		return numberExistResponse, err
	}

	//Запись контакта в БД
	err = dao.UpdateBucket(in.Contact)
	if err != nil {
		log.Println ("\t error in Update DB: ", err)
		return badServerResponse, err
	}

	return goodCreateResponse, nil
}

//Получение всех контактов в БД. Возвращает поток
func (s *server) GetAllPhonebook(key *empty.Empty, stream rrs.PhoneBook_GetAllPhonebookServer) error {
	log.Println("Get all contacts...")

	//Открытие БД
	err, db := dao.OpenDB()
	if err != nil {
		log.Println("\t error in Open DB: ", err)
		return err
	}
	defer db.Close()//После выполнения команды - закрытие БД

	//Создание корзины, если ее нет
	err = dao.CreateBucket()
	if err != nil {
		log.Println ("\t error in Create Bucket in DB: ", err)
		return err
	}

	//получение всех записей из корзины и их трансляция
	contact := &rrs.ContactResponse{Contact: &models.Contact{}}
	err = db.View(func(tx *bolt.Tx) error { //Открытие корзины для просмотра
		book := tx.Bucket([]byte(dao.NameBucket))
		book.ForEach(func(k, v []byte) error { //Итерация полученной корзины
			err := proto.Unmarshal(v, contact.Contact) //анмаршал значение корзины
			if err != nil{
				return err
			}
			//Передача потока данных
			if err := stream.Send(contact); err != nil {
				log.Println("\t error streem: ", err)
				return err
			}
			return nil
		})
		return nil
	})
	if err != nil {
		log.Println("\t error in View DB: ", err)
		return err
	}
	return nil
}

//Удаление контакта. Возврат Статуса true, если удаление прошло успешно, иначе не нашел контакт
func (s *server) DeleteContact(ctx context.Context, in *rrs.NumberRequest) (*rrs.MessResponse, error) {
	log.Println("Delete contact...")

	//Проверка валидности номера
	if !utils.CheckValidNumber(in.Phone.Number) {		//проверка на формат номера
		log.Println ("\t error in check validate number: ", errors.New("Invalid number"))
		return badClientResponse, nil
	}

	//Открытие БД
	err, db := dao.OpenDB()
	if err != nil {
		log.Println("\t error in Open DB: ", err)
		return badServerResponse, err
	}
	defer db.Close() //После выполнения команды - закрытие БД

	//Создание корзины, если ее нет
	err = dao.CreateBucket()
	if err != nil {
		log.Println ("\t error in Create Bucket in DB: ", err)
		return badServerResponse, err
	}

	//проверка на существование номера
	if err, bool, _ := dao.ViewBucketForCoincidence(in.Phone.Number); bool == false {
		if err != nil {
			log.Println("\t error in view DB: ", err)
			return badServerResponse, err
		}
		log.Println("\t error in view DB: ", errors.New("This number does not exist"))
		return numberNotFoundResponse, err
	}

	//Удаление элемента из корзины в БД
	err = dao.DeleteElementInBucket(in.Phone.Number)
	if err != nil{
		log.Println("\t error in delete busket: ", err)
		return badServerResponse, err
	}
	return goodResponse, nil
}

//Поиск контакта. Возврат полной струтктуры, если совпадение есть, иначе возврат пустой структуры
func (s *server) SearchContact(ctx context.Context, in *rrs.NumberRequest) (*rrs.ContactResponse, error) {
	log.Println("Search contact...")

	response := &rrs.ContactResponse{}

	//Проверка валидности номера
	if !utils.CheckValidNumber(in.Phone.Number) {		//проверка на формат номера
		log.Println ("\t error in check validate number: ", errors.New("Invalid number"))
		return response, errors.New("400 Invalid number")
	}

	//Открытие БД
	err, db := dao.OpenDB()
	if err != nil {
		log.Println("\t error in Open DB: ", err)
		return response, err
	}
	defer db.Close() //После выполнения команды - закрытие БД

	//Создание корзины, если ее нет
	err = dao.CreateBucket()
	if err != nil {
		log.Println ("\t error in Create Bucket in DB: ", err)
		return response, err
	}

	//проверка на существование номера и возврат полученного значения
	if err, bool, item := dao.ViewBucketForCoincidence(in.Phone.Number); bool == true {
		if err != nil {
			log.Println("\t error in view DB: ", err)
			return response, err
		}
		response.Contact = item
		return response, err
	}
	return response, errors.New("404 This number does not exist")
}

//Изменение имени контакта. Возврат статуса true, если котакт найден и изменен, иначе контакт не найден
func (s *server) UpdateContact(ctx context.Context, in *rrs.ContactRequest) (*rrs.MessResponse, error) {
	log.Println("Update name contact...")

	//Проверка валидности номера
	if !utils.CheckValidNumber(in.Contact.Number) {		//проверка на формат номера
		log.Println ("\t error in check validate number: ", errors.New("Invalid number"))
		return badClientResponse, nil
	}

	//Открытие БД
	err, db := dao.OpenDB()
	if err != nil {
		log.Println("\t error in Open DB: ", err)
		return badServerResponse, err
	}
	defer db.Close() //После выполнения команды - закрытие БД

	//Создание корзины, если ее нет
	err = dao.CreateBucket()
	if err != nil {
		log.Println ("\t error in Create Bucket in DB: ", err)
		return badServerResponse, err
	}

	//Проверка на существование записи. Если существует, тогда изменяем
	if err, bool, _ := dao.ViewBucketForCoincidence(in.Contact.Number); bool == true {
		if err != nil {
			log.Println("\t error in view DB: ", err)
			return badServerResponse, err
		}
		err = dao.UpdateBucket(in.Contact)
		if err != nil {
			log.Println ("\t error in Update DB: ", err)
			return badServerResponse, err
		}
		return goodResponse, nil

	}

	log.Println("\t error in view DB: ", errors.New("This number does not exist"))
	return numberNotFoundResponse, err
}