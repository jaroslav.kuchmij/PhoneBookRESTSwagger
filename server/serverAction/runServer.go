package serverAction

import (
	"log"
	"net/http"
	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"gitlab.com/jaroslav.kuchmij/PhoneBookRESTSwagger.git/rrs"
	"context"
	"google.golang.org/grpc"
	"fmt"
	"net"
	"github.com/gorilla/mux"
	"gitlab.com/jaroslav.kuchmij/PhoneBookRESTSwagger.git/ui"
)

type server struct {}
const (
	portGRCP = ":50051"
	portHTTP = ":8080"
	network = "tcp"
)

func RunGRPC() {
	log.Println("Start server...")
	//Открытие порта
	lis, err := net.Listen(network, portGRCP)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	// Creates a new gRPC server
	s := grpc.NewServer()
	rrs.RegisterPhoneBookServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}


func RunHTTP() {
	//Создание swagger
	router := mux.NewRouter()
	FS := swaggerui.GetFS()
	router.PathPrefix("/swaggerui/").
		Handler(http.StripPrefix("/swaggerui/", FS)).
		Name("swaggerui")

	//создание контекста
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	clientPort := fmt.Sprintf("localhost%s", portGRCP)
	opts := []grpc.DialOption{grpc.WithInsecure()}
	// Creates a new HTTP server
	mux := runtime.NewServeMux()
	if err := rrs.RegisterPhoneBookHandlerFromEndpoint(ctx, mux, clientPort, opts); err != nil {
		log.Fatalf("failed to start HTTP server: %v", err)
	}

	//ПОдключение swagger к REST серверу
	router.PathPrefix("/phonebook/").Handler(mux)

	log.Printf("HTTP Listening on %s\n", portHTTP)
	//Запуск HTTP сервера
	log.Fatal(http.ListenAndServe(portHTTP, router))
}