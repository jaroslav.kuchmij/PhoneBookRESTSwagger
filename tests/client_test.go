package tests

import (
	"testing"
	"log"
	. "github.com/smartystreets/goconvey/convey"
	"google.golang.org/grpc"
	"gitlab.com/jaroslav.kuchmij/phoneBookGRPC.git/models"
	"context"
	"github.com/boltdb/bolt"
	"gitlab.com/jaroslav.kuchmij/phoneBookGRPC.git/utils"
	"io"
	"path/filepath"
)

const (
	address = "localhost:50051"
)

func TestAddContact(t *testing.T)  {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := models.NewPhoneBookClient(conn)

	incomingValue := &models.ContactFullRequest{
		Number: "+380981111111",
		Name: "Name",
		Address: &models.ContactFullRequest_Address{
			Street: "Street",
			NumberHouse: "0",
			NumberFlat: "0",
		},
	}

	//Проверка добавления нового корректного контакта
	Convey("AddContact: good value", t, func() {

		res, err := client.AddContact(context.Background(), incomingValue)
		So(err, ShouldBeNil)
		So(res.Number, ShouldEqual, incomingValue.Number)
		So(res.Status, ShouldBeTrue)
	})

	//Проверка добавление контакта с неверным форматом телефона
	Convey("AddContact: wrong format number", t, func() {
		incomingValue := &models.ContactFullRequest{
			Number: "test",
			Name: "Name",
			Address: &models.ContactFullRequest_Address{
				Street: "Street",
				NumberHouse: "0",
				NumberFlat: "0",
			},
		}
		//expectedValue := &models.ContactResponse{}
		res, err := client.AddContact(context.Background(), incomingValue)
		So(err, ShouldNotBeNil)
		So(res, ShouldBeNil)
	})

	//Проверка на добавление контакта, который уже существует
	Convey("AddContact: existing number", t, func() {
		incomingValue := &models.ContactFullRequest{
			Number: "+380981111111",
			Name: "Name",
			Address: &models.ContactFullRequest_Address{
				Street: "Street",
				NumberHouse: "0",
				NumberFlat: "0",
			},
		}
		res, err := client.AddContact(context.Background(), incomingValue)
		So(err, ShouldBeNil)
		So(res.Number, ShouldEqual, incomingValue.Number)
		So(res.Status, ShouldBeFalse)
	})

	//Очистка тестовой БД
	path, err := filepath.Abs("../server/Test.db")
	if err != nil {
		return
	}
	db, err := bolt.Open(path, 0600, nil)
	if err != nil{
		return
	}
	defer db.Close() //После выполнения команды - закрытие БД
	err = db.Update(func(tx *bolt.Tx) error { //зменение корзины
		bckt := tx.Bucket([]byte(utils.NameBucket)) //Получение корзины по наименованию
		err := bckt.Delete([]byte(incomingValue.Number)) //Удаление записи в корзине по ключу
		return err
	})
}

func TestGetAllPhonebook(t *testing.T)  {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := models.NewPhoneBookClient(conn)

	incomingValue := &models.ContactFullRequest{
		Number: "+380981111111",
		Name: "Name",
		Address: &models.ContactFullRequest_Address{
			Street: "Street",
			NumberHouse: "0",
			NumberFlat: "0",
		},
	}
	res, err := client.AddContact(context.Background(), incomingValue)
	if err != nil {
		log.Println("error in addContact():", err)
		return
	}
	if res.Status == false {
		log.Println("This number exist:", res.Number)
		return
	}

	//Проверка на корректное получения списка контактов
	Convey("GetAllPhonebook: good value", t, func() {
		key := &models.ContactKeyword{Keyword: ""}
		stream, err := client.GetAllPhonebook(context.Background(), key)
		for {
			contact, err := stream.Recv() //Получение элемента из потока
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("%v.GetCustomers(_) = _, %v", client, err)
			}

			So(contact.Number, ShouldEqual, incomingValue.Number)
			So(contact.Name, ShouldEqual, incomingValue.Name)
			So(contact.Address.Street, ShouldEqual, incomingValue.Address.Street)
			So(contact.Address.NumberFlat, ShouldEqual, incomingValue.Address.NumberFlat)
			So(contact.Address.NumberHouse, ShouldEqual, incomingValue.Address.NumberHouse)
		}
		So(err, ShouldBeNil)
	})

	//Очистка тестовой БД
	path, err := filepath.Abs("../server/Test.db")
	if err != nil {
		return
	}
	db, err := bolt.Open(path, 0600, nil)
	if err != nil{
		return
	}
	defer db.Close() //После выполнения команды - закрытие БД
	err = db.Update(func(tx *bolt.Tx) error { //зменение корзины
		bckt := tx.Bucket([]byte(utils.NameBucket)) //Получение корзины по наименованию
		err := bckt.Delete([]byte(incomingValue.Number)) //Удаление записи в корзине по ключу
		return err
	})
}

func TestDeleteContact(t *testing.T) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := models.NewPhoneBookClient(conn)

	incomingValue := &models.ContactFullRequest{
		Number: "+380981111111",
		Name: "Name",
		Address: &models.ContactFullRequest_Address{
			Street: "Street",
			NumberHouse: "0",
			NumberFlat: "0",
		},
	}

	res, err := client.AddContact(context.Background(), incomingValue)
	if err != nil {
		log.Println("error in addContact():", err)
		return
	}
	if res.Status == false {
		log.Println("This number exist:", res.Number)
		return
	}

	//Проверка при удалении контакта переданного с неправильным форматом номера
	Convey("DeleteContact: wrong format number", t, func() {
		number := &models.NumberRequest{
			Number: "test",
		}
		res, err := client.DeleteContact(context.Background(), number)
		So(err, ShouldNotBeNil)
		So(res, ShouldBeNil)
	})

	//Проверка при удалении на отсутствие нужного контакта
	Convey("DeleteContact: does not exist number", t, func() {
		number := &models.NumberRequest{
			Number: "+380981111112",
		}
		res, err := client.DeleteContact(context.Background(), number)
		So(err, ShouldBeNil)
		So(res.Number, ShouldEqual, number.Number)
		So(res.Status, ShouldBeFalse)
	})

	//Проверка при удалении на корректное удаление правильного контакта
	Convey("DeleteContact: good value", t, func() {
		number := &models.NumberRequest{
			Number: "+380981111111",
		}
		res, err := client.DeleteContact(context.Background(), number)
		So(err, ShouldBeNil)
		So(res.Number, ShouldEqual, number.Number)
		So(res.Status, ShouldBeTrue)
	})

	//Проверка телефонной книги после даления контактов
	Convey("DeleteContact: check value into db", t, func() {
		key := &models.ContactKeyword{Keyword: ""}
		stream, err := client.GetAllPhonebook(context.Background(), key)
		for {
			contact, err := stream.Recv() //Получение элемента из потока
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("%v.GetCustomers(_) = _, %v", client, err)
			}

			So(contact.Number, ShouldBeNil)
			So(contact.Name, ShouldBeNil)
			So(contact.Address.Street, ShouldBeNil)
			So(contact.Address.NumberFlat, ShouldBeNil)
			So(contact.Address.NumberHouse, ShouldBeNil)
		}
		So(err, ShouldBeNil)
	})

	//Очистка тестовой БД
	path, err := filepath.Abs("../server/Test.db")
	if err != nil {
		return
	}
	db, err := bolt.Open(path, 0600, nil)
	if err != nil{
		return
	}
	defer db.Close() //После выполнения команды - закрытие БД
	err = db.Update(func(tx *bolt.Tx) error { //зменение корзины
		bckt := tx.Bucket([]byte(utils.NameBucket)) //Получение корзины по наименованию
		err := bckt.Delete([]byte(incomingValue.Number)) //Удаление записи в корзине по ключу
		return err
	})
}

func TestSearchContact(t *testing.T)  {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := models.NewPhoneBookClient(conn)

	incomingValue := &models.ContactFullRequest{
		Number: "+380981111111",
		Name: "Name",
		Address: &models.ContactFullRequest_Address{
			Street: "Street",
			NumberHouse: "0",
			NumberFlat: "0",
		},
	}

	res, err := client.AddContact(context.Background(), incomingValue)
	if err != nil {
		log.Println("error in addContact():", err)
		return
	}
	if res.Status == false {
		log.Println("This number exist:", res.Number)
		return
	}

	//Проверка поиска контакта с передачей правильного номера
	Convey("SearchContact: good value", t, func() {
		number := &models.NumberRequest{
			Number: "+380981111111",
		}
		res, err := client.SearchContact(context.Background(), number)
		So(err, ShouldBeNil)
		So(res.Number, ShouldEqual, incomingValue.Number)
		So(res.Name, ShouldEqual, incomingValue.Name)
		So(res.Address.Street, ShouldEqual, incomingValue.Address.Street)
		So(res.Address.NumberHouse, ShouldEqual, incomingValue.Address.NumberHouse)
		So(res.Address.NumberFlat, ShouldEqual, incomingValue.Address.NumberFlat)
	})

	//Проверка поиска с передачей неправильного формата номера
	Convey("SearchContact: wrong format number", t, func() {
		number := &models.NumberRequest{
			Number: "test",
		}
		res, err := client.SearchContact(context.Background(), number)
		So(err, ShouldNotBeNil)
		So(res, ShouldBeNil)
	})

	//Проверка поиска с передачей контакта которого нет в тел книге
	Convey("SearchContact: does not exist number", t, func() {
		number := &models.NumberRequest{
			Number: "+380981111112",
		}
		res, err := client.SearchContact(context.Background(), number)
		So(err, ShouldBeNil)
		So(res.Number, ShouldBeEmpty)
		So(res.Name, ShouldBeEmpty)
	})

	//Очистка тестовой БД
	path, err := filepath.Abs("../server/Test.db")
	if err != nil {
		return
	}
	db, err := bolt.Open(path, 0600, nil)
	if err != nil{
		return
	}
	defer db.Close() //После выполнения команды - закрытие БД
	err = db.Update(func(tx *bolt.Tx) error { //зменение корзины
		bckt := tx.Bucket([]byte(utils.NameBucket)) //Получение корзины по наименованию
		err := bckt.Delete([]byte(incomingValue.Number)) //Удаление записи в корзине по ключу
		return err
	})
}

func TestUpdateName(t *testing.T) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := models.NewPhoneBookClient(conn)

	incomingValue := &models.ContactFullRequest{
		Number: "+380981111111",
		Name: "Name",
		Address: &models.ContactFullRequest_Address{
			Street: "Street",
			NumberHouse: "0",
			NumberFlat: "0",
		},
	}

	res, err := client.AddContact(context.Background(), incomingValue)
	if err != nil {
		log.Println("error in addContact():", err)
		return
	}
	if res.Status == false {
		log.Println("This number exist:", res.Number)
		return
	}

	//Проверка при изменении имени контакта на то что номера нет
	Convey("UpdateName: does not exist number", t, func() {
		number := &models.UpdateNameRequest{
			Number: "+380981111112",
			Name: "test",
		}
		res, err := client.UpdateName(context.Background(), number)
		So(err, ShouldBeNil)
		So(res.Number, ShouldEqual, number.Number)
		So(res.Status, ShouldBeFalse)
	})

	//Проверка при изменении имени на не верный формат номера
	Convey("UpdateName: wrong format number", t, func() {
		number := &models.UpdateNameRequest{
			Number: "test",
			Name: "test",
		}
		res, err := client.UpdateName(context.Background(), number)
		So(err, ShouldNotBeNil)
		So(res, ShouldBeNil)
	})

	//Проверка при измененни контакта правильного значения
	Convey("UpdateName: good values", t, func() {
		number := &models.UpdateNameRequest{
			Number: "+380981111111",
			Name: "test",
		}
		res, err := client.UpdateName(context.Background(), number)
		So(err, ShouldBeNil)
		So(res.Number, ShouldEqual, number.Number)
		So(res.Status, ShouldBeTrue)
	})

	//Проверка телефонной книги после изменения контакта
	Convey("DeleteContact: check value into db", t, func() {
		key := &models.ContactKeyword{Keyword: ""}
		stream, err := client.GetAllPhonebook(context.Background(), key)
		for {
			contact, err := stream.Recv() //Получение элемента из потока
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("%v.GetCustomers(_) = _, %v", client, err)
			}

			So(contact.Number, ShouldEqual, incomingValue.Number)
			So(contact.Name, ShouldEqual, "test")
			So(contact.Address.Street, ShouldEqual, incomingValue.Address.Street)
			So(contact.Address.NumberFlat, ShouldEqual, incomingValue.Address.NumberFlat)
			So(contact.Address.NumberHouse, ShouldEqual, incomingValue.Address.NumberHouse)
		}
		So(err, ShouldBeNil)
	})

	//Очистка тестовой БД
	path, err := filepath.Abs("../server/Test.db")
	if err != nil {
		return
	}
	db, err := bolt.Open(path, 0600, nil)
	if err != nil{
		return
	}
	defer db.Close() //После выполнения команды - закрытие БД
	err = db.Update(func(tx *bolt.Tx) error { //зменение корзины
		bckt := tx.Bucket([]byte(utils.NameBucket)) //Получение корзины по наименованию
		err := bckt.Delete([]byte(incomingValue.Number)) //Удаление записи в корзине по ключу
		return err
	})
}

func TestUpdateAddress(t *testing.T) {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	client := models.NewPhoneBookClient(conn)

	incomingValue := &models.ContactFullRequest{
		Number: "+380981111111",
		Name: "Name",
		Address: &models.ContactFullRequest_Address{
			Street: "Street",
			NumberHouse: "0",
			NumberFlat: "0",
		},
	}

	res, err := client.AddContact(context.Background(), incomingValue)
	if err != nil {
		log.Println("error in addContact():", err)
		return
	}
	if res.Status == false {
		log.Println("This number exist:", res.Number)
		return
	}

	//Проверка при изменении адреса на то что номера нет
	Convey("UpdateAddress: does not exist number", t, func() {
		number := &models.UpdateAddressRequest{
			Number: "+380981111112",
			Address: &models.UpdateAddressRequest_Address{
				Street: "test",
				NumberHouse: "test",
				NumberFlat: "test",
			},
		}
		res, err := client.UpdateAddress(context.Background(), number)
		So(err, ShouldBeNil)
		So(res.Number, ShouldEqual, number.Number)
		So(res.Status, ShouldBeFalse)
	})

	//Проверка при изменении адреса на не верный формат номера
	Convey("UpdateAddress: wrong format number", t, func() {
		number := &models.UpdateAddressRequest{
			Number: "test",
			Address: &models.UpdateAddressRequest_Address{
				Street: "test",
				NumberHouse: "test",
				NumberFlat: "test",
			},
		}
		res, err := client.UpdateAddress(context.Background(), number)
		So(err, ShouldNotBeNil)
		So(res, ShouldBeNil)
	})

	//Проверка при измененни контакта правильного значения
	Convey("UpdateAddress: good values", t, func() {
		number := &models.UpdateAddressRequest{
			Number: "+380981111111",
			Address: &models.UpdateAddressRequest_Address{
				Street: "test",
				NumberHouse: "test",
				NumberFlat: "test",
			},
		}
		res, err := client.UpdateAddress(context.Background(), number)
		So(err, ShouldBeNil)
		So(res.Number, ShouldEqual, number.Number)
		So(res.Status, ShouldBeTrue)
	})

	//Проверка телефонной книги после изменения контакта
	Convey("DeleteAddress: check value into db", t, func() {
		key := &models.ContactKeyword{Keyword: ""}
		stream, err := client.GetAllPhonebook(context.Background(), key)
		for {
			contact, err := stream.Recv() //Получение элемента из потока
			if err == io.EOF {
				break
			}
			if err != nil {
				log.Fatalf("%v.GetCustomers(_) = _, %v", client, err)
			}

			So(contact.Number, ShouldEqual, incomingValue.Number)
			So(contact.Name, ShouldEqual, incomingValue.Name)
			So(contact.Address.Street, ShouldEqual, "test")
			So(contact.Address.NumberFlat, ShouldEqual, "test")
			So(contact.Address.NumberHouse, ShouldEqual, "test")
		}
		So(err, ShouldBeNil)
	})

	//Очистка тестовой БД
	path, err := filepath.Abs("../server/Test.db")
	if err != nil {
		return
	}
	db, err := bolt.Open(path, 0600, nil)
	if err != nil{
		return
	}
	defer db.Close() //После выполнения команды - закрытие БД
	err = db.Update(func(tx *bolt.Tx) error { //зменение корзины
		bckt := tx.Bucket([]byte(utils.NameBucket)) //Получение корзины по наименованию
		err := bckt.Delete([]byte(incomingValue.Number)) //Удаление записи в корзине по ключу
		return err
	})
}